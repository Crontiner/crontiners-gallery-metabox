<?php
/**
 * Plugin Name: Crontiner's Gallery metabox
 * Plugin URI:
 * Description:
 * Version: 1.0
 * Author: Crontiner
 * Author URI:
 */


/**********************/
/*   GLOBAL VALUES    */
/**********************/

define( 'CGM_TEXTDOMAIN', 'crontiners-gallery-metabox' );

/**/


class load_language {
	public function __construct() {
		add_action('init', array($this, 'load_my_transl'));
	}
	public function load_my_transl() {
		load_plugin_textdomain(CGM_TEXTDOMAIN, FALSE, dirname(plugin_basename(__FILE__)).'/languages/');
	}
}
$zzzz = new load_language;


/********************************/
/*         CSS & JQUERY         */
/********************************/

add_action('admin_enqueue_scripts', 'galeria_metabox_enqueue_scripts_admin');
function galeria_metabox_enqueue_scripts_admin () {

	/* CSS */
	wp_enqueue_style('gallery_metabox_font_awesome_css', plugin_dir_url( __FILE__ ).'lib/font_awesome/css/font-awesome.min.css');
	wp_enqueue_style('gallery_metabox_fancybox_css', plugin_dir_url( __FILE__ ).'lib/fancybox/dist/jquery.fancybox.min.css');
	wp_enqueue_style('galeria_metabox_admin_css', plugin_dir_url( __FILE__ ).'css/admin.css?v='. rand(9999, 1) );

	/* JS */
	wp_enqueue_script( 'galeria_metabox_fancybox_js', plugin_dir_url( __FILE__ ) . 'lib/fancybox/dist/jquery.fancybox.min.js' );
	wp_enqueue_script( 'galeria_metabox_admin_js', plugin_dir_url( __FILE__ ) . 'js/admin.js' );

	if ( !did_action( 'wp_enqueue_media' ) ) {
		wp_enqueue_media();
	}
}

/**/



/*************************/
/*    OTHER FUNCTIONS    */
/*************************/


function flatten_array($mArray) {
    $sArray = array();

    foreach ($mArray as $row) {
        if ( !(is_array($row)) ) {
            if($sArray[] = $row){
            }
        } else {
            $sArray = array_merge($sArray,flatten_array($row));
        }
    }
    return $sArray;
}

function gallery_get_converted_categories( $a, $level) {
	$term_editor =
		'<span class="term_controller">
			<div class="parent-term-nav">
				<div class="menu-item to_parent" data-balloon="Move to parent term" data-balloon-pos="up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></div>
				<div class="menu-item to_child" data-balloon="Move to child term" data-balloon-pos="down"><i class="fa fa-angle-double-down" aria-hidden="true"></i></div>
			</div>

			<div class="move-up-and-down">
				<div class="menu-item move_up" data-balloon="Move to top" data-balloon-pos="up"><i class="fa fa-angle-up" aria-hidden="true"></i></div>
				<div class="menu-item move_down" data-balloon="Move to down" data-balloon-pos="down"><i class="fa fa-angle-down" aria-hidden="true"></i></div>
			</div>

			<div class="other">
				<div class="menu-item rename" data-balloon="Rename category" data-balloon-pos="up"><i class="fa fa-pencil" aria-hidden="true"></i></div>
				<div class="menu-item remove" data-balloon="Delete category" data-balloon-pos="down"><i class="fa fa-trash" aria-hidden="true"></i></div>
			</div>
		</span>';

	 $r = '<ul class="main-ul">';
	 foreach ( $a as $i ) {
		 if ($i['parent_id'] == $level ) {

				$r = $r . '<li>
											<div class="term" data-term-id="'. $i['id'] .'">'
													.'<input type="text" readonly="true" value="'. $i['title'] .'">'
													. $term_editor
											.'</div>'.
											gallery_get_converted_categories( $a, $i['id'] ) .
									 '</li>';
		 }
	 }
	 $html_result = $r . '</ul>';
	 return $html_result;
}


/**/



/**************************/
/*        SIDEBARS	  */
/**************************/



/**/



/**********************/
/*    SHORTCODE       */
/**********************/



/**/



/************************/
/*       METABOX        */
/************************/

add_action( 'add_meta_boxes', 'gallery_metabox_custom_meta_box' );
function gallery_metabox_custom_meta_box($post) {
	add_meta_box('galeria_metabox', __('Attach images and documents', CGM_TEXTDOMAIN), 'galeria_metabox_function', 'page', 'normal', 'default');
}

function galeria_metabox_function() {
	?>
	<div class="header-section">
		<ul class="main-functions">
			<li data-screen="gallery-screen" class="active"><i class="fa fa-picture-o" aria-hidden="true"><span>36</span></i></li>
			<li data-screen="file-list-screen" ><i class="fa fa-file-text-o" aria-hidden="true"><span>21</span></i></li>
			<li data-screen="history-screen"><i class="fa fa-history" aria-hidden="true"></i></li>
			<li data-screen="remove-all-screen"><i class="fa fa-trash" aria-hidden="true"></i></li>
		</ul>
	</div>

	<div class="main-right-menu">
		<ul>
			<li data-screen="gallery-screen" class="active"><i class="fa fa-files-o" aria-hidden="true"></i></li>
			<li data-screen="terms-settings-screen"><i class="fa fa-sitemap" aria-hidden="true"></i></li>
			<li data-screen="settings-screen"><i class="fa fa-cog" aria-hidden="true"></i></li>
			<li data-screen="info-screen"><i class="fa fa-info-circle" aria-hidden="true"></i></li>
		</ul>

		<div class="upload-box"><i class="fa fa-upload" aria-hidden="true"></i></div>
	</div>


	<div class="carousel-control active">
		<div class="left"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
		<div class="right"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
	</div>


	<div class="content-wrapper">

		<!-- Screen: gallery -->
		<?php include 'inc/gallery-screen.php'; ?>

		<!-- Screen: file list view -->
		<?php include 'inc/file-list-screen.php'; ?>

		<!-- Screen: Terms settings -->
		<?php include 'inc/terms-settings-screen.php'; ?>

		<!-- Screen: Settings -->
		<div class="settings-screen">
			Do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</div>

		<!-- Screen: Info -->
		<div class="info-screen">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit.
		</div>

		<!-- Screen: History -->
		<div class="history-screen">
			Ut enim ad minim veniam, quis nostrud.
		</div>

		<!-- Screen: Remove all -->
		<div class="remove-all-screen">
			<div class="content">
				<h3>Are your sure want to delete all attachments from this gallery?</h3>
				<a href="#" class="button button-primary button-large" id="remove_all" /><i class="fa fa-trash" aria-hidden="true"></i> Remove all</a>
				<a href="#" class="button button-primary button-large" /><i class="fa fa-times-circle" aria-hidden="true"></i> Cancel</a>
			</div>
		</div>

		<!-- Screen: Image edit (AJAX) -->
		<?php //include 'inc/image-edit-screen.php'; ?>

	</div>


	<span class="gradient-overlay"></span>
	<div class="clear"></div>
	<?php
}


/**/



/*************************************/
/*    SAVE POST / METABOX / etc...   */
/*************************************/

add_action('save_post', 'gallery_metabox_save_metadata');
function gallery_metabox_save_metadata($post_id) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;

		if ( isset($_POST['save_categories_data']) && !empty($_POST['save_categories_data']) ) {
			$categories_data = json_decode($_POST['save_categories_data']);

			foreach ($categories_data as $key => $value) {
				# code...
			}
		}
}

/**/



/*******************/
/*      CPT        */
/*******************/



/**/



/*******************************/
/*          ADD ACTION         */
/*******************************/



/**/



/*******************************/
/*          DO ACTION          */
/*******************************/



/**/


/*******************************/
/*           FILTER            */
/*******************************/

add_action('admin_footer', 'gallery_admin_footer');
function gallery_admin_footer() {
	?>
	<script type="text/javascript">
		jQuery(function($) {
			var $metabox = $('#galeria_metabox.postbox');

			/* insert loading html */

			$metabox.find('h2.hndle').append('<span class="save-indicator"><span>Saving...</span><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></span>');

			/* TODO: majd leszedni! */
			$metabox.addClass('saving');

		});
	</script>
	<?php
}

/**/


/*******************************/
/*						AJAX*/					 */
/*******************************/



/**/
