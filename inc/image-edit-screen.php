<!-- Screen: Image edit -->

<div class="image-edit-screen">

  <div class="left_side">
    <div class="img-preview">
      <img src="http://ideiglenes.paulovics.hu/wp-content/uploads/2017/09/batch_A-New-World.jpg" />
    </div>

    <ul class="attachment-info-content">
  		<li>Upload date <b>2017.05.31 20:37</b></li>
  		<li>File format: <b>image/jpeg</b></li>
  		<li>Uploader: <b><a target="_blank" href="http://wiki.aurettoworks.com/wp-admin/profile.php">tibi</a></b></li>
  		<a target="_blank" href="http://wiki.aurettoworks.com/wp-admin/post.php?post=3654&amp;action=edit">Media Library</a>
  	</ul>
  </div>

  <div class="right_side">

     <ul class="edit-menu">
        <li data-content-id="data-editing" class="active"><span>Data editing</span></li>
        <li data-content-id="display-settings"><span>Display settings</span></li>
        <li data-content-id="categories"><span>Categories</span></li>
     </ul>

     <div class="close_btn">
       <span><i class="fa fa-times-circle-o" aria-hidden="true"></i></span>
     </div>

     <div class="edit-content-wrapper">

      <!-- Data editing -->

      <div class="edit-content data-editing active">
        <div class="block">
          <p class="block-title">Add/edit media data <small>The image details will be changed permanently.</small></p>

          <label for="basic_media_title">Title:</label>
          <input type="text" value="" name="basic_media_title" id="basic_media_title">

          <label for="basic_media_desc">Description:</label>
          <textarea name="basic_media_desc" id="basic_media_desc"></textarea>
        </div>

        <div class="block">
          <p>Add/edit gallery data <span>The image settings will be modified within this gallery. If the image is used elsewhere as well, those settings will remain untouched.</span></p>

          <label for="added_media_title">Title:</label>
          <input type="text" value="" name="added_media_title" id="added_media_title">

          <label for="added_media_desc">Description:</label>
          <textarea name="added_media_desc" id="added_media_desc"></textarea>
        </div>

        <div class="block">
          <label for="added_media_url">URL:</label>
          <input type="text" value="" name="added_media_url" id="added_media_url">
        </div>

        <input type="submit" class="button button-primary button-large" value="Save" name="save_edit_content" />
      </div>


      <!-- Display settings -->

      <div class="edit-content display-settings">
        <p class="block-title">Edit settings displayed with Timthumb</p>

        <form action="" method="post">

          <div class="img_crop_setting active img_crop">
            <table>
              <tbody>
                <tr>
                  <td><input type="radio" value="cropped" name="img_crop_setting" id="crop_img" checked="checked"></td>
                  <th><label for="crop_img">Crop image</label></th>
                </tr>
                <tr>
                  <td><input type="radio" value="full_size" name="img_crop_setting" id="show_whole_img"></td>
                  <th><label for="show_whole_img">Show whole image</label></th>
                </tr>
              </tbody>
            </table>
          </div>

          <div class="img_crop_setting active cutting_location">
            <div class="left_side">
              Set visible area:
            </div>
            <div class="right_side">
              <div class="img_cutting_point">
                <input type="radio" value="tl" name="focus_point"> <!-- top-left -->
                <input type="radio" value="t" name="focus_point"> <!-- top -->
                <input type="radio" value="tr" name="focus_point"> <!-- top-right -->
                <input type="radio" value="l" name="focus_point"> <!-- left -->
                <input type="radio" value="c" name="focus_point" checked="checked"> <!-- center -->
                <input type="radio" value="r" name="focus_point"> <!-- right -->
                <input type="radio" value="bl" name="focus_point"> <!-- bottom-left -->
                <input type="radio" value="b" name="focus_point"> <!-- bottom -->
                <input type="radio" value="br" name="focus_point"> <!-- bottom-right -->
              </div>
            </div>
          </div>

          <div class="img_crop_setting bg_color">
            <p>Default background</p>
            <table>
              <tbody>
                <tr>
                  <td><input type="radio" value="transparent" id="bg_color_transparent" checked="checked" name="bg_color"></td>
                  <th><label for="bg_color_transparent">Transparent (only with PNG images)</label></th>
                </tr>
                <tr>
                  <td><input type="radio" value="white" id="bg_color_white" name="bg_color"></td>
                  <th><label for="bg_color_white">White</label></th>
                </tr>
                <tr>
                  <td><input type="radio" value="black" id="bg_color_black" name="bg_color"></td>
                  <th><label for="bg_color_black">Black</label></th>
                </tr>
                <tr>
                  <td><input type="radio" value="custom_color" id="bg_color_custom_color" name="bg_color"></td>
                  <th>
                    <label for="bg_color_custom_color">Other:</label>
                    <input type="text" placeholder="#9cc303" name="custom_bg_color" value="">
                  </th>
                </tr>
              </tbody>
            </table>
          </div>

          <div class="img_crop_setting active get_preview">
            <div class="preview_btn button button-large"><i class="fa fa-eye"></i>Preview</div>
            <div class="desc">Theaspect ratio of the preview image may differ from the aspect ratio shown on the webpage.</div>
          </div>

        </form>
        <div class="clear"></div>

        <input type="submit" class="button button-primary button-large" value="Save" name="save_edit_content" />
      </div>


      <!-- Categories -->

      <div class="edit-content categories">
        dolor sit amet

        <input type="submit" class="button button-primary button-large" value="Save" name="save_edit_content" />
      </div>

     </div>

  </div>


</div>
