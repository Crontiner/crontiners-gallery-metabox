<?php
	$a = array (
			'0' => array (
					'id' => 'uncategorized',
					'title' => "Uncategorized",
					'parent_id' => 'NULL',
			),

			'1' => array (
					'id' => 1,
					'title' => "Animal",
					'parent_id' => 'NULL',
			),
							'3' => array (
									'id' => 3,
									'title' => "Birds",
									'parent_id' => 1,
							),
							'4' => array (
									'id' => 4,
									'title' => "Mammals",
									'parent_id' => 1,
							),
											'6' => array (
													'id' => 6,
													'title' => "Elephant",
													'parent_id' => 4,
											),
											'7' => array (
													'id' => 7,
													'title' => "Mouse",
													'parent_id' => 4,
											),
							'5' => array (
									'id' => 5,
									'title' => "Reptiles",
									'parent_id' => 1,
							),
			'2' => array (
					'id' => 2,
					'title' => "Plants",
					'parent_id' => 'NULL',
			),
	);
?>


<!-- Screen: Terms settings -->
<div class="terms-settings-screen">

	<div class="set-new-term">
		<form action="" method="post">

			<?php
				if ( !empty($a) ) {
					$categ_array_temp = array();
					foreach ($a as $key => $categ_array) {
						$categ_array_temp [$categ_array['title']]= array( 'term_id' => $categ_array['id'] );
					}
					ksort($categ_array_temp);

					$options_html = "";
					foreach ($categ_array_temp as $categ_title => $categ_array) {
						$options_html .= '<option value="'. $categ_array['term_id'] .'">'. $categ_title .'</option>';
					}
					?>

					<label for="parent_categ_select">Parent Categories</label><br>
					<select id="parent_categ_select">
						<option value="">None</option>
						<?php echo $options_html; ?>
					</select>
					<?php
				}
			?>

			<input	type="text" value="" name="new_term" placeholder="New Category Name" />
			<input type="submit" class="button button-primary button-large" value="Add New Category" name="" />
		</form>
	</div>


<?php

$categories_data = json_decode('[{"id":"uncategorized","title":"Uncategorized","parent_id":""},{"id":"1","title":"Animal","parent_id":""},{"id":"3","title":"Birds","parent_id":"3"},{"id":"4","title":"Mammals","parent_id":"4"},{"id":"6","title":"Elephant","parent_id":"6"},{"id":"7","title":"Mouse","parent_id":"7"},{"id":"5","title":"Reptiles","parent_id":"5"},{"id":"2","title":"Plants","parent_id":""}]');


$categories_data_temp = array();
$categories_data_temp []= array (
															'id' => 'uncategorized',
															'title' => "Uncategorized",
															'parent_id' => 'NULL',
														);

foreach ($categories_data as $key => $term_data) {

	$id 				= (int) $term_data->id;
	$title 			= esc_attr(stripslashes($term_data->title));
	$parent_id 	= (int) $term_data->parent_id;

	if ( $parent_id > 0 ) {  }
	else { $parent_id = 'NULL'; }

	var_dump($id);

	if ( ($id > 0) && !empty($title) ) {
		$categories_data_temp []= array (
																	'id' => $id,
																	'title' => $title,
																	'parent_id' => $parent_id,
																);
	}
}

echo "<pre>";
var_dump($categories_data_temp);
echo "</pre>";

?>


	<div class="terms-view">
		<?php echo gallery_get_converted_categories( $a, 'NULL' ); ?>
		<input type="submit" class="button button-primary button-large" value="Save" name="save_categories" />
		<input type="hidden" value="" name="save_categories_data" />
	</div>

</div>
