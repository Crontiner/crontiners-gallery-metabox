<!-- Screen: file list view -->
<div class="file-list-screen">

  <ul>
    <?php for ($i=0; $i < 20; $i++) { ?>
    <li>
      <div class="filename">Filename.doc</div>
      <div class="overlay">
        <div class="full-view-btn"><i class="fa fa-eye" aria-hidden="true"></i></div>
        <div class="edit-btn"><i class="fa fa-pencil" aria-hidden="true"></i></div>
        <div class="trash-btn"><i class="fa fa-trash" aria-hidden="true"></i></div>
      </div>
    </li>
    <?php } ?>
  </ul>

</div>
