<!-- Screen: gallery -->
<div class="gallery-screen">

  <ul>

    <?php for ($i=0; $i < 20; $i++) { ?>
    <li attachment-id="3651">
      <div class="img" style="background: url(http://ideiglenes.paulovics.hu/wp-content/uploads/2017/09/batch_A-New-World-150x150.jpg);">
        <div class="overlay">
          <div class="trash-btn"><i class="fa fa-trash" aria-hidden="true"></i></div>
          <div class="full-view-btn" href="http://ideiglenes.paulovics.hu/wp-content/uploads/2017/09/batch_A-New-World.jpg" data-caption="My caption" data-fancybox="images" data-width="1024" data-height="1024"><i class="fa fa-search" aria-hidden="true"></i></div>
          <div class="edit-btn"><i class="fa fa-pencil" aria-hidden="true"></i></div>
        </div>
      </div>
    </li>
    <?php } ?>
  </ul>

</div>
