jQuery(function($) {
	var $metabox = $('#galeria_metabox.postbox');

	function mgp_terms_rebuild_data_level() {
		$('div.terms-settings-screen .terms-view ul').each(function(key, index) {
			if ( $(this).html() == "" ) { $(this).remove(); }
			else {
				$(this).attr('data-level', 'id-'+ key);
			}
		});
	}


	/* main-functions menu */

	$metabox.find('.header-section ul.main-functions li').click(function(){
		var $this = $(this);

		$metabox.find('.main-right-menu ul li.active:not([data-screen="gallery-screen"])').removeClass('active');
		$metabox.find('.main-right-menu ul li[data-screen="gallery-screen"]').addClass('active');

		if ( !$this.hasClass('active') ) {
			$this.closest('ul').find('li.active').removeClass('active');
			$this.addClass('active');

			$metabox.attr('data-screen', $this.data('screen') );
		}
	});


	/* main-right-menu */

	$metabox.find('.main-right-menu ul li').click(function(){
		var $this = $(this);

		if ( $this.data('screen') == 'gallery-screen' ) {
			$metabox.find('.header-section ul.main-functions li.active:not([data-screen="gallery-screen"])').removeClass('active');
			$metabox.find('.header-section ul.main-functions li[data-screen="gallery-screen"]').addClass('active');
		} else {
			$metabox.find('.header-section ul.main-functions li.active').removeClass('active');
		}

		if ( !$this.hasClass('active') ) {
			$this.closest('ul').find('li.active').removeClass('active');
			$this.addClass('active');

			$metabox.attr('data-screen', $this.data('screen') );
		}
	});


	/* Show relevant content */

	if ( $metabox.find('.main-right-menu ul li.active').length ) {
		var activeMenuItem = $metabox.find('.main-right-menu ul li.active').data('screen');
		$metabox.attr('data-screen', activeMenuItem);
	}


	/* Stop scrolling */

	$metabox.find('.content-wrapper').hover (
	  function() {
	    $('body').addClass('stop_scrolling');
	  }, function() {
	    $('body').removeClass('stop_scrolling');
	  }
	);


	/* Image edit screen: Change content */

	$metabox.find('.image-edit-screen ul.edit-menu li').click(function(){
		var $this = $(this);
		var cid = $this.data('content-id');
		var $target = $this.closest('.image-edit-screen').find('.edit-content-wrapper .edit-content.'+ cid);

		if ( $this.hasClass('active') ) {  }
		else {
			$this.closest('ul').find('li.active').removeClass('active');
			$this.addClass('active');
		}

		if ( $target.hasClass('active') ) {  }
		else {
			$this.closest('.image-edit-screen').find('.edit-content-wrapper .edit-content.active').removeClass('active');
			$target.addClass('active');
		}
	});


	/* Image edit screen: Change display settings */

	$metabox.find('.image-edit-screen .display-settings .img_crop_setting.img_crop input[type="radio"]').click(function(){
		var $this = $(this);
		var activeItem = $this.closest('.img_crop_setting').find('input[type="radio"]:checked').val();

		if ( activeItem == 'full_size' ) {
			$this.closest('.display-settings').find('.img_crop_setting.cutting_location').removeClass('active');
			$this.closest('.display-settings').find('.img_crop_setting.bg_color').addClass('active');
		} else {
			$this.closest('.display-settings').find('.img_crop_setting.cutting_location').addClass('active');
			$this.closest('.display-settings').find('.img_crop_setting.bg_color').removeClass('active');
		}
	});





	/*
	 * Select/Upload image(s) event
	 */
	$('body').on('click', '.main-right-menu div.upload-box', function(e){
	    e.preventDefault();

	        var button = $(this),
	            custom_uploader = wp.media({
	        title: 'Insert image',
	        library : {
	            // uncomment the next line if you want to attach image to the current post
	            // uploadedTo : wp.media.view.settings.post.id,
	            //type : 'image'
	        },
	        button: {
	            text: 'Use this image' // button label text
	        },
	        multiple: true // for multiple image selection set to true
	    }).on('select', function() { // it also has "open" and "close" events

					/*
					var attachment = custom_uploader.state().get('selection').first().toJSON();
	        $(button).removeClass('button').html('<img class="true_pre_image" src="' + attachment.url + '" style="max-width:95%;display:block;" />').next().val(attachment.id).next().show();
					*/

					// if you sen multiple to true, here is some code for getting the image IDs

					var attachments = custom_uploader.state().get('selection'),
	            attachment_ids = new Array(),
	            i = 0;

	        attachments.each(function(attachment) {
	            attachment_ids[i] = attachment['id'];
	            console.log( attachment_ids[i] );
	            i++;
	        });
	    })
	    .open();
	});


	$metabox.find('div.gallery-screen ul, div.file-list-screen ul').sortable({
		items: "li",
		placeholder: "sortable-placeholder",
		revert: 200,
		stop: function( event, ui ) {
			console.dir( event );
			console.dir( ui );
		}
	});


	/* Terms Settings Screen: term controller */

	$('body').on('click', 'div.terms-settings-screen .terms-view ul li .term_controller div.menu-item', function() {
		var $this = $(this);

		mgp_terms_rebuild_data_level();

		var currentDataLevel = $this.closest('ul').attr('data-level');
		var parentDataLevel = $this.closest('ul:not([data-level="'+ currentDataLevel +'"])').attr('data-level');
		var childDataLevel = $this.closest('li').find('ul').attr('data-level');

		var li = $this.closest('li')[0].outerHTML;

		if ( $this.hasClass('to_parent') ) {

			if ( parentDataLevel != undefined && parentDataLevel != "" ) {

				$this.closest('li').fadeOut(function(){
					$(this).remove();
					$( 'ul[data-level="'+ parentDataLevel +'"]' ).prepend( li );
				});
			}

		} else if ( $this.hasClass('to_child') ) {

			if ( $this.closest('li').next().length ) {
				$this.closest('li').fadeOut(function(){
					if ( $(this).next().find('ul').length ) {
						$( $(this).next().find('ul').first() ).prepend( li );
					} else {
						$(this).next().append( '<ul>'+ li +'</ul>' );
					}
					$(this).remove();
				});
			}

		} else if ( $this.hasClass('move_up') ) {

			$this.closest('li').prev().before( $this.closest('li') );

		} else if ( $this.hasClass('move_down') ) {

			$this.closest('li').next().after( $this.closest('li') );

		} else if ( $this.hasClass('rename') ) {

			$this.closest('.term').find('input').prop('readonly', false).focus();

		} else if ( $this.hasClass('remove') ) {

			$this.closest('.term').fadeOut(function(){
				var $this = $(this);

				if ( $this.closest('li').find('ul').length < 1 ) {
					$this.unwrap('<li></li>');
				}

				$this.remove();
				mgp_terms_rebuild_data_level();
			});

		}
	});


	/* Terms Settings Screen: save new term name after rename term */

	$('#galeria_metabox.postbox div.terms-settings-screen .terms-view ul li .term input[type="text"]').focusout(function() {
		var $this = $(this);
		$this.attr('value', $this.val());
	});


	/* Terms Settings Screen: set active item */

	$('body').on('click', 'div.terms-settings-screen .terms-view ul li .term', function() {
		var $this = $(this);
		var termID = $this.data('term-id');

		if ( !$this.hasClass('active') ) {
			$this.addClass('active');
		} else {
			$this.removeClass('active');
		}

		$('#galeria_metabox.postbox .terms-view ul li .term').each(function(){
			if ( termID != $(this).attr('data-term-id') ) {
				$(this).removeClass('active');
				$(this).find('input').prop('readonly', true);
			}
		});
	});


	/* Terms Settings Screen: Add New Category */

	$('#galeria_metabox.postbox div.terms-settings-screen .set-new-term input[type="submit"]').click(function(e){
		e.preventDefault();
		var $this = $(this);
		var $termsSettingsScreen = $this.closest('.terms-settings-screen');

		var parentCateg = $termsSettingsScreen.find('select#parent_categ_select').val();
		var newTerm = $termsSettingsScreen.find('input[name="new_term"]').val();
		var sampleCategory = $('div.terms-settings-screen .terms-view ul li .term[data-term-id="uncategorized"]').closest('li')[0].outerHTML;
		var newCategID = Math.floor((Math.random() * 9999999) + 1);

		sampleCategory = sampleCategory.replace('data-term-id', 'data-new-item="true" data-term-id');

		if ( newTerm == "" ) {
			$termsSettingsScreen.find('input[name="new_term"]').addClass('error');
		} else {
			$termsSettingsScreen.find('input[name="new_term"]').removeClass('error');

			if ( parentCateg != undefined && parentCateg != "" ) {

				// Set new term under parent

					// Parent term has child category
					if ( $('div.terms-settings-screen .terms-view ul li .term[data-term-id="'+ parentCateg +'"]').closest('li').find('ul').html() != "" ) {

						$('div.terms-settings-screen .terms-view ul li .term[data-term-id="'+ parentCateg +'"]').closest('li').find('ul').first().prepend( sampleCategory );
					} else {
					// Create new child category

						$('div.terms-settings-screen .terms-view ul li .term[data-term-id="'+ parentCateg +'"]').closest('li').find('ul').html( sampleCategory );
					}
			} else {
				$( sampleCategory ).insertAfter( $('div.terms-settings-screen .terms-view ul li .term[data-term-id="uncategorized"]').closest('li') )
			}


			// replace name & ID
			var $newCategItem = $('div.terms-settings-screen .terms-view ul li .term[data-new-item="true"]');
			$newCategItem.attr('data-term-id', newCategID);
			$newCategItem.find('input[type="text"]').attr('value', newTerm);
			$newCategItem.removeAttr('data-new-item');

		}
	});


	/* Terms Settings Screen: Collecting categories before saving */

	$('#galeria_metabox.postbox div.terms-settings-screen .terms-view input[name="save_categories"]').click(function(e){
		e.preventDefault();
		var termsArray = [];

		$(this).closest('.terms-view').find('ul li .term').each(function(){
			var $this = $(this);
			var termID = $this.attr('data-term-id');
			var parentTermID = "";
			var title = $this.find('input[type="text"]').val();

			if ( $this.closest('li ul li').find('.term').length ) {
				var parentTermID_temp = $this.closest('li ul li').find('.term').first().attr('data-term-id');
				if ( parentTermID_temp > 0 ) {
					parentTermID = parentTermID_temp;
				}
			}

			termsArray.push({
			 'id' : termID,
			 'title' : title,
			 'parent_id' : parentTermID
			});
		});

		$('#galeria_metabox.postbox div.terms-settings-screen .terms-view input[name="save_categories_data"]').attr('value', JSON.stringify(termsArray) );

		// push save button
		//$('#publishing-action input[name="save"]').click();
	});

});
